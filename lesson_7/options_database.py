import tkinter as tk


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Демо опции")
        self.option_add("*font", "helvetica 10")
        self.option_add("*header.font", "helvetica 18 bold")
        self.option_add("*subtitle.font", "helvetica 14 italic")
        self.option_add("*Button.foreground", "blue")
        self.option_add("*Button.background", "white")
        self.option_add("*Button.activeBackground", "gray")
        self.option_add("*Button.activeForeground", "black")

        self.create_label(name="header", text="Это 'header'")
        self.create_label(name="subtitle", text="Это 'subtitle'")
        self.create_label(text="Это параграф")
        self.create_label(text="Это следующий параграф")
        self.create_button(text="Больше...")

    def create_label(self, **options):
        tk.Label(self, **options).pack(padx=20, pady=5, anchor=tk.W)

    def create_button(self, **options):
        tk.Button(self, **options).pack(padx=5, pady=5, anchor=tk.E)


if __name__ == "__main__":
    app = App()
    app.mainloop()
